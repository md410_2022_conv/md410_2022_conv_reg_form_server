from fastapi import FastAPI
from pydantic import BaseModel
from rich import print

from md410_2022_conv_common import models, sqs

from datetime import datetime
import os
from typing import Optional

DEBUG = os.getenv("DEBUG")

app = FastAPI()


class RegFormData(BaseModel):
    main_first_names: str
    main_last_name: str
    main_name_badge: str
    main_club: str
    main_cell: str
    main_email: str
    main_dietary: Optional[str]
    main_disability: Optional[str]
    main_first_mdc: Optional[str]
    main_mjf_lunch: Optional[str]
    main_pdg_dinner: Optional[str]
    main_lpe_breakfast: Optional[str]
    partner: str
    partner_lion_first_names: str
    partner_lion_last_name: str
    partner_lion_name_badge: str
    partner_lion_club: str
    partner_lion_cell: str
    partner_lion_email: str
    partner_lion_dietary: Optional[str]
    partner_lion_disability: Optional[str]
    partner_lion_first_mdc: Optional[str]
    partner_lion_mjf_lunch: Optional[str]
    partner_lion_pdg_dinner: Optional[str]
    partner_lion_lpe_breakfast: Optional[str]
    partner_non_lion_first_names: str
    partner_non_lion_last_name: str
    partner_non_lion_name_badge: str
    partner_non_lion_cell: str
    partner_non_lion_email: str
    partner_non_lion_dietary: Optional[str]
    partner_non_lion_disability: Optional[str]
    partner_non_lion_first_mdc: Optional[str]
    partner_non_lion_mjf_lunch: Optional[str]
    partner_non_lion_pdg_dinner: Optional[str]
    partner_non_lion_lpe_breakfast: Optional[str]
    partner_non_lion_partner_program: Optional[str]
    pins: str
    windbreaker_s: str
    windbreaker_m: str
    windbreaker_l: str
    windbreaker_xl: str
    windbreaker_2xl: str
    windbreaker_3xl: str
    windbreaker_4xl: str
    windbreaker_5xl: str
    transport: str
    ip: str
    user_agent: str
    referrer: str


class RegFormModel(BaseModel):
    number: int
    title: Optional[str]
    email: Optional[str]
    name: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    company: Optional[str]
    summary: Optional[str]
    body: Optional[dict]
    data: Optional[RegFormData]
    created_at: Optional[str]
    human_fields: Optional[dict]
    ordered_human_fields: Optional[list]
    id: Optional[str]
    form_id: Optional[str]
    site_url: Optional[str]
    form_name: Optional[str]


@app.post("/reg_form")
def reg_form(reg_form: RegFormModel):
    """Accept and process reg_form data"""
    data = reg_form.data
    attendees = [
        models.AttendeeModel(
            first_names=data.main_first_names,
            last_name=data.main_last_name,
            name_badge=data.main_name_badge,
            cell=data.main_cell,
            email=data.main_email,
            dietary=data.main_dietary or None,
            disability=data.main_disability or None,
            first_mdc=bool(data.main_first_mdc),
            mjf_lunch=bool(data.main_mjf_lunch),
            pdg_dinner=bool(data.main_pdg_dinner),
            lpe_breakfast=bool(data.main_lpe_breakfast),
            lion=True,
            club=data.main_club,
        )
    ]
    if data.partner in ("partner_lion", "partner_non_lion"):
        attendees.append(
            models.AttendeeModel(
                first_names=getattr(data, f"{data.partner}_first_names"),
                last_name=getattr(data, f"{data.partner}_last_name"),
                name_badge=getattr(data, f"{data.partner}_name_badge"),
                cell=getattr(data, f"{data.partner}_cell"),
                email=getattr(data, f"{data.partner}_email"),
                dietary=getattr(data, f"{data.partner}_dietary") or None,
                disability=getattr(data, f"{data.partner}_disability") or None,
                first_mdc=bool(getattr(data, f"{data.partner}_first_mdc")),
                mjf_lunch=bool(getattr(data, f"{data.partner}_mjf_lunch")),
                pdg_dinner=bool(getattr(data, f"{data.partner}_pdg_dinner")),
                lpe_breakfast=bool(getattr(data, f"{data.partner}_lpe_breakfast")),
                lion=data.partner == "partner_lion",
                club=data.partner_lion_club if data.partner == "partner_lion" else None,
                partner_program=bool(data.partner_non_lion_partner_program)
                if data.partner == "partner_non_lion"
                else None,
            )
        )
    windbreakers = []
    for size in ("s", "m", "l", "xl", "2xl", "3xl", "4xl", "5xl"):
        try:
            num = int(getattr(data, f"windbreaker_{size}"))
            if num:
                windbreakers.extend([size.upper()] * num)
        except Exception:
            continue
    items = models.RegistrationItems(
        reg=len(attendees),
        pins=int(data.pins or 0),
        windbreakers=windbreakers,
        transport=int(data.transport or 0),
    )

    registration = models.Registration(
        attendees=attendees,
        items=items,
        timestamp=datetime.fromisoformat(
            reg_form.created_at[:-1],
        ),
    )
    json = registration.json()
    sqs.send_reg_form_data(json)
    if DEBUG:
        print(json)
    return True
